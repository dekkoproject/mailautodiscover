/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import Ubuntu.Components 1.2
import MailAutoDiscover 1.0

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "mailautodiscover.dpniel"

    width: units.gu(50)
    height: units.gu(60)

    Page {
        title: i18n.tr("MailAutoDiscover")



        AutoDiscover {
            id: autoDiscover
            onInvalidMailAddress: console.log("Not a valid mailaddress: ", address)
            onFailed: {
                ff.text = "Lookup failed for " + tf.text
                console.log("Lookup Failed")
            }
            onNoNetworkAvailable: console.log("No network available, lookup aborted")
            onStatusChanged: {
                switch(status) {
                case AutoDiscover.INVALID:
                    console.log("Status >> Invalid")
                    break;
                case AutoDiscover.NEW_REQUEST:
                    console.log("Status >> Building initial request")
                    break;
                case AutoDiscover.REQUEST_AUTOCONFIG:
                    console.log("Status >> Requesting autoconfig.example.com")
                    break;
                case AutoDiscover.REQUEST_AUTOCONFIG_WELLKNOWN:
                    console.log("Status >> Requesting www.example.com/.wellknown/*")
                    break;
                case AutoDiscover.REQUEST_AUTOCONFIG_ISPDB:
                    console.log("Status >> Requesting autoconfig from mozilla db")
                    break;
                case AutoDiscover.REQUEST_SRV:
                    console.log("Status >> Requesting SRV record")
                    break
                case AutoDiscover.REQUEST_FAILED:
                    console.log("Status >> Request failed")
                    break
                case AutoDiscover.REQUEST_SUCCEEDED:
                    console.log("Status >> Request succeeded")
                    break
                case AutoDiscover.BUILDING_RESULT:
                    console.log("Status >> Building result")
                    break;
                }
            }
            onSuccess: {
                ih.text = "<b>IMAP host: </b>" + serverConfig.imapHostName
                ip.text = "<b>IMAP port: </b>" + serverConfig.imapPort
                im.text = "<b>IMAP socket method: </b>" + serverConfig.toMap().imap.method
                sh.text = "<b>SMTP host: </b>" + serverConfig.smtpHostName
                sp.text = "<b>SMTP port: </b>" + serverConfig.smtpPort
                sm.text = "<b>SMTP socket method: </b>" + serverConfig.toMap().smtp.method
                console.log("Success: ", serverConfig.imapHostName)
            }
        }

        ActivityIndicator {
            anchors.centerIn: parent
            visible: running
            running: autoDiscover.inProgress
        }

        Column {
            anchors {
                left: parent.left
                top: parent.top
                right: parent.right
                margins: units.gu(2)
            }
            spacing: units.gu(1)
            TextField {
                id: tf
                width: parent.width
            }
            Button {
                width: parent.width
                text: "Look for details"
                color: UbuntuColors.green
                onClicked: {
                    cleanup()
                    autoDiscover.lookUp(tf.text)
                }
            }
            Label{id:ff}
            Label{id:ih}
            Label{id:ip}
            Label{id:im}
            Label{id:sh}
            Label{id:sp}
            Label{id:sm}
            // Example showing the EmailValidator singleton
            Label{
                text: EmailValidator.validate(tf.text) ? "Email address is valid" : "Email address is not Valid"
            }
        }

    }
    function cleanup() {
        ff.text = ""
        ih.text = ""
        ip.text = ""
        im.text = ""
        sh.text = ""
        sp.text = ""
        sm.text = ""
    }
}

