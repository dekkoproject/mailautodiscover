### QtQuick plugin for discovery of mail server configurations ###

This is a simple plugin that looks for mail server configurations using a combination of the processes outlined [here](https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Autoconfiguration) and [here](https://tools.ietf.org/html/rfc6186)

### To run the example app ###

```
#!bash

git clone https://bitbucket.org/dekkoproject/mailautodiscover.git
cd mailautodiscover
mkdir __build
cd __build
cmake ..
make run
```
You may need to install the libconnectivity-qt1-dev package if you don't already have it installed.

To run the unit tests it's the same as above apart from the make command should be 

```
#!bash

make check
```